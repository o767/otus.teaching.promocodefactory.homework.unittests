﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Common;

public static class DataContextInMemoryFactory
{
    public static DataContext Create()
    {
        var dbContextOptions = new DbContextOptionsBuilder<DataContext>()
            .UseInMemoryDatabase(databaseName: "MyBlogDb")
            .Options;

        return new DataContext(dbContextOptions);
    }

    public static async void Destroy(DataContext context)
    {
        await context.Database.EnsureCreatedAsync();
        await context.DisposeAsync();
    }
}