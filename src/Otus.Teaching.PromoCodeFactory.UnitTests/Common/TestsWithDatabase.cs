﻿using System;
using Otus.Teaching.PromoCodeFactory.DataAccess;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.Common;

public abstract class TestsWithDatabase: IDisposable
{
    protected DataContext Context { get; }

    protected TestsWithDatabase()
    {
        Context = DataContextInMemoryFactory.Create();
    }

    public void Dispose()
    {
        DataContextInMemoryFactory.Destroy(Context);
    }
}