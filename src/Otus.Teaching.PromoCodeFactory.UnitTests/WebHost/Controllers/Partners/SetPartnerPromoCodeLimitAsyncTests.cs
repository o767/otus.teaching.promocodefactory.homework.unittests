﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

public class SetPartnerPromoCodeLimitAsyncTests
{
    private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
    private readonly PartnersController _partnersController;
    public readonly DbContextOptions<DataContext> _dbContextOptions;

    public SetPartnerPromoCodeLimitAsyncTests()
    {
        var fixture = new Fixture().Customize(new AutoMoqCustomization());
        _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
        _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();

        _dbContextOptions = new DbContextOptionsBuilder<DataContext>()
            .UseInMemoryDatabase(databaseName: "MyBlogDb")
            .Options;
    }

    public Partner CreateBasePartner()
    {
        var partner = new Partner()
        {
            Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
            Name = "Суперигрушки",
            IsActive = true,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            }
        };

        return partner;
    }

    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
    {
        // Arrange
        var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
        var partnerId = Guid.NewGuid();
        Partner partner = null;

        _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partnerId)).ReturnsAsync(partner);

        // Act
        var result =
            await _partnersController.SetPartnerPromoCodeLimitAsync(partnerId, setPartnerPromoCodeLimitRequest);

        // Assert
        result.Should().BeAssignableTo<NotFoundResult>();
    }

    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
    {
        // Arrange
        var setPartnerPromoCodeLimitRequest = new Fixture().Create<SetPartnerPromoCodeLimitRequest>();
        var partner = CreateBasePartner();
        partner.IsActive = false;

        _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

        // Act
        var result =
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

        // Assert
        result.Should().BeAssignableTo<BadRequestObjectResult>();
    }

    [Theory]
    [InlineData(0)]
    [InlineData(1)]
    public async void SetPartnerPromoCodeLimitAsync_SetLimit_ChangeNumberIssuedPromoCodes(int limit)
    {
        // Arrange
        var partner = CreateBasePartner();
        partner.NumberIssuedPromoCodes = 10;
        var setPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(x => x.Limit, limit).Create();

        var expectedResult = limit == 0 ? partner.NumberIssuedPromoCodes : 0;

        _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

        // Act
        var result =
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

        // Assert
        result.Should().BeAssignableTo<CreatedAtActionResult>();
        partner.NumberIssuedPromoCodes.Should().Be(expectedResult);
    }

    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_SetLimit_DisableThePreviousLimit()
    {
        // Arrange
        var partner = CreateBasePartner();
        var oldPromoCodeLimit = partner.PartnerLimits.Single();
        var setPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(x => x.Limit, 10).Create();

        _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

        // Act
        var result =
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

        // Assert
        result.Should().BeAssignableTo<CreatedAtActionResult>();
        oldPromoCodeLimit.CancelDate.Should().NotBeNull();
    }

    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_LimitLessThan0_ReturnsBadRequest()
    {
        // Arrange
        var setPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(x => x.Limit, -1).Create();
        var partner = CreateBasePartner();

        _partnersRepositoryMock.Setup(x => x.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

        // Act
        var result =
            await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);

        // Assert
        result.Should().BeAssignableTo<BadRequestObjectResult>();
    }
}