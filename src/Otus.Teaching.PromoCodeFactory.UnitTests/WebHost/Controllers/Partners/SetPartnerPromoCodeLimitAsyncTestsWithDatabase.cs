﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;
using Otus.Teaching.PromoCodeFactory.UnitTests.Common;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Xunit;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners;

public class SetPartnerPromoCodeLimitAsyncTestsWithDatabase: TestsWithDatabase
{
    private readonly IRepository<Partner> _partnersRepository;

    public SetPartnerPromoCodeLimitAsyncTestsWithDatabase()
    {
        _partnersRepository = new EfRepository<Partner>(Context);
    }
    public Partner CreateBasePartner()
    {
        var partner = new Partner()
        {
            Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
            Name = "Суперигрушки",
            IsActive = true,
            PartnerLimits = new List<PartnerPromoCodeLimit>()
            {
                new PartnerPromoCodeLimit()
                {
                    Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                    CreateDate = new DateTime(2020, 07, 9),
                    EndDate = new DateTime(2020, 10, 9),
                    Limit = 100
                }
            }
        };

        return partner;
    }

    [Fact]
    public async void SetPartnerPromoCodeLimitAsync_SetLimit_SavedTheNewLimitToDatabase()
    {
        // Arrange
        var partner = CreateBasePartner();
        var partnersController = new PartnersController(_partnersRepository);
        await _partnersRepository.AddAsync(partner);

        var setPartnerPromoCodeLimitRequest = new Fixture().Build<SetPartnerPromoCodeLimitRequest>().With(x => x.Limit, 10).Create();

        // Act
        var result =
            await partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, setPartnerPromoCodeLimitRequest);
        var newLimit = (await _partnersRepository.GetByIdAsync(partner.Id)).PartnerLimits.Single(x => !x.CancelDate.HasValue).Limit;

        // Assert
        result.Should().BeAssignableTo<CreatedAtActionResult>();
        newLimit.Should().Be(setPartnerPromoCodeLimitRequest.Limit);
    }
}